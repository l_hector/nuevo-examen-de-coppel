package lugo.arredondo.hector.examencoppel.handlers;

import android.view.View;
import android.widget.Toast;

/**
 * Created by Hector on 12/11/2017.
 */

public interface LoginHandler {
    public void login(View view);
}
