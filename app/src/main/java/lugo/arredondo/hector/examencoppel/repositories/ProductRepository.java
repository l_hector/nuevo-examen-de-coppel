package lugo.arredondo.hector.examencoppel.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;
import java.util.logging.Handler;

import lugo.arredondo.hector.examencoppel.data_base.DataBase;
import lugo.arredondo.hector.examencoppel.helpers.Resource;
import lugo.arredondo.hector.examencoppel.models.Product;
import lugo.arredondo.hector.examencoppel.services.ProductService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hector on 12/11/2017.
 */

public class ProductRepository {
    private final static String TAG = ProductRepository.class.getSimpleName();

    private ProductService mService;
    private DataBase mDataBase;

    public ProductRepository(ProductService service, DataBase productDAO) {
        mService = service;
        mDataBase = productDAO;
    }

    public LiveData<Resource<List<Product>>> getProducts() {
        final MutableLiveData<Resource<List<Product>>> data = new MutableLiveData<>();
        data.setValue(Resource.loading(null));

        mService.consultProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if (response.isSuccessful()) {
                    data.setValue(Resource.success(response.body()));

                    mDataBase.productDAO().deleteAll();
                    mDataBase.productDAO().saveAll(response.body());
                } else {
                    Log.e(TAG, "Error al consultar productos." + response.errorBody());
                    data.setValue(Resource.error("Error al consultar productos", null));
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.e(TAG, "Error al consultar productos.", t);
                data.setValue(Resource.error("Error al consultar productos.", null));
            }
        });

        return data;
    }

    public LiveData<List<Product>> getProductLocal() {
        return mDataBase.productDAO().findAll();
    }

    public LiveData<Product> getProductById(int id) {
        return mDataBase.productDAO().find(id);
    }

    public LiveData<Resource<Product>> saveProductLocal(Product product) {
        final MutableLiveData<Resource<Product>> data = new MutableLiveData<>();
        data.setValue(Resource.loading(null));

        new android.os.Handler().postDelayed(() -> {
            product.setLocal(true);
            mDataBase.productDAO().save(product);

            data.setValue(Resource.success("Producto guardado.", product));
        }, 3000);

        return data;
    }

    public LiveData<Resource<Product>> saveProduct(Product product) {
        final MutableLiveData<Resource<Product>> data = new MutableLiveData<>();
        data.setValue(Resource.loading(null));

        mService.saveProduct(product).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if (response.isSuccessful() && response.body() != null) {
                    data.setValue(Resource.success(response.body()));
                    mDataBase.productDAO().save(product);
                } else {
                    Log.e(TAG, "Ocurrio un error al guardar. " + response.errorBody() );
                    data.setValue(Resource.error("Ocurrio un problema al guardar." ,response.body()));
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.e(TAG, "Ocurrio un error al guardar.", t);
                data.setValue(Resource.error("Ocurrio un problema al guardar." , null));
            }
        });

        return data;
    }

    public LiveData<Resource<Product>> deleteProduct(Product product) {
        final MutableLiveData<Resource<Product>> data = new MutableLiveData<>();
        data.setValue(Resource.loading(null));

        new android.os.Handler().postDelayed(() -> {
            product.setLocal(true);
            mDataBase.productDAO().delete(product);

            data.setValue(Resource.success("Producto " + product.getName() + " eliminado.", product));
        }, 3000);

        return data;
    }
}
