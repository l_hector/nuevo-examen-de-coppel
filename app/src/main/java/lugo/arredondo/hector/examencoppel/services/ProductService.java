package lugo.arredondo.hector.examencoppel.services;

import android.arch.persistence.room.Delete;

import java.util.List;

import lugo.arredondo.hector.examencoppel.models.Product;
import lugo.arredondo.hector.examencoppel.models.views.ProductViewModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Hector on 12/11/2017.
 */

public interface ProductService {
    @GET("products")
    public Call<List<Product>> consultProducts();
    @POST("products")
    public Call<Product> saveProduct(@Body Product product);
    @POST("products/{id}")
    public Call<Boolean> deleteProdeuct(@Path("id") int id);
}
