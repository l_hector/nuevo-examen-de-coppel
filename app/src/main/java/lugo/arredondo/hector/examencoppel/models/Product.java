package lugo.arredondo.hector.examencoppel.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import lugo.arredondo.hector.examencoppel.BR;

/**
 * Created by Hector on 12/11/2017.
 */

@Entity
public class Product extends BaseObservable {
    @PrimaryKey (autoGenerate = true)
    private int id;
    @SerializedName("nombre")
    private String name;
    @SerializedName("imagenGrande")
    private String img;
    @SerializedName("descripcion")
    private String descripction;
    private boolean local;

    public Product() {
        this.local = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Bindable
    public String getDescripction() {
        return descripction;
    }

    public void setDescripction(String descripction) {
        this.descripction = descripction;
        notifyPropertyChanged(BR.descripction);
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }
}
