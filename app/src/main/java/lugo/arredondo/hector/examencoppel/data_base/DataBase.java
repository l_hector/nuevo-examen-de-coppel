package lugo.arredondo.hector.examencoppel.data_base;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import lugo.arredondo.hector.examencoppel.dao.ProductDAO;
import lugo.arredondo.hector.examencoppel.models.Product;

/**
 * Created by Hector on 12/11/2017.
 */

@Database(entities = {Product.class}, version = 1)
public abstract class DataBase extends RoomDatabase{
    public abstract ProductDAO productDAO();
    private static DataBase INSTANCE;

    public static DataBase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), DataBase.class, "profile-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
