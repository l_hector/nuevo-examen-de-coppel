package lugo.arredondo.hector.examencoppel.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by Hector on 13/11/2017.
 */

public class Utils {
    public static boolean isConnected(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        Log.e("wifi available ", (wifi.isAvailable() && wifi.isConnected()) + "");
        Log.e("mobile available ", (mobile.isAvailable() && mobile.isConnected()) + "");
        return ((wifi.isAvailable() && wifi.isConnected()) || (mobile.isAvailable() && mobile.isConnected()));
    }
}
