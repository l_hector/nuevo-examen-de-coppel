package lugo.arredondo.hector.examencoppel.handlers;

import android.view.View;

import lugo.arredondo.hector.examencoppel.models.Product;

/**
 * Created by Hector on 12/11/2017.
 */

public interface ProductRowHandle {
    void deleteProduct(Product item);
    void onClickProduct(Product item);
}
