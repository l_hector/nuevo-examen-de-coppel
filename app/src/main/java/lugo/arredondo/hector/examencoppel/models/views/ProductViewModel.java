package lugo.arredondo.hector.examencoppel.models.views;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import java.util.List;

import lugo.arredondo.hector.examencoppel.data_base.DataBase;
import lugo.arredondo.hector.examencoppel.helpers.ApiFactory;
import lugo.arredondo.hector.examencoppel.helpers.Resource;
import lugo.arredondo.hector.examencoppel.models.Product;
import lugo.arredondo.hector.examencoppel.repositories.ProductRepository;
import lugo.arredondo.hector.examencoppel.services.ProductService;

import static lugo.arredondo.hector.examencoppel.helpers.Resource.error;

/**
 * Created by Hector on 12/11/2017.
 */

public class ProductViewModel extends ViewModel {
    private LiveData<List<Product>> products;
    private ProductRepository repository;

    public ProductViewModel(Context context) {
        this.repository = new ProductRepository(ApiFactory.createInstance(ProductService.class),
                DataBase.getAppDatabase(context));
    }

    public void init() {
        if (products != null) {
            // ViewModel is created per Fragment so
            // we know the userId won't change
            return;
        }

        products = new LiveData<List<Product>>() {
        };
        products = repository.getProductLocal();
    }

    public LiveData<List<Product>> getProducts() {
        return products;
    }

    public LiveData<Resource<List<Product>>> updateProduct() {
        return repository.getProducts();
    }

    public LiveData<Resource<Product>> saveProductLocal(Product product) {
        return repository.saveProductLocal(product);
    }

    public LiveData<Resource<Product>> deleteProduct(int idProduct) {
        Product product = repository.getProductById(idProduct).getValue();

        return repository.deleteProduct(product);
    }

    public LiveData<Resource<Product>> deleteProduct(Product product) {
        return repository.deleteProduct(product);
    }
}
