package lugo.arredondo.hector.examencoppel.models;

import android.arch.lifecycle.LiveData;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import lugo.arredondo.hector.examencoppel.BR;

/**
 * Created by Hector on 12/11/2017.
 */

public class User extends BaseObservable {
    private String user;
    private String pass;

    public User() {
    }

    public User(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    @Bindable
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
        notifyPropertyChanged(BR.user);
    }

    @Bindable
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
        notifyPropertyChanged(BR.pass);
    }
}
