package lugo.arredondo.hector.examencoppel.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import lugo.arredondo.hector.examencoppel.R;
import lugo.arredondo.hector.examencoppel.databinding.RowProductBinding;
import lugo.arredondo.hector.examencoppel.handlers.ProductRowHandle;
import lugo.arredondo.hector.examencoppel.models.Product;

/**
 * Created by Hector on 12/11/2017.
 */

public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder> {
    private final static String TAG = ProductRecyclerViewAdapter.class.getSimpleName();

    private Context mContext;
    private final List<Product> mValues;
    private ProductRowHandle handle;
    private LayoutInflater layoutInflater;

    public ProductRecyclerViewAdapter(Context context, List<Product> values) {
        mContext = context;
        mValues = values;
        layoutInflater = LayoutInflater.from(context);

        if (context instanceof ProductRowHandle) {
            handle = ((ProductRowHandle) context);
        } else {
            Log.e(TAG, "handle no implementado");

            handle = new ProductRowHandle() {
                @Override
                public void deleteProduct(Product item) {
                    Log.e("ProductRow item", "Delete " + item.getId());
                }

                @Override
                public void onClickProduct(Product item) {
                    Log.e("ProductRow item", "Click " + item.getId());
                }
            };
        }
    }

    @Override
    public ProductRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        final RowProductBinding binding = DataBindingUtil.inflate(layoutInflater,
                R.layout.row_product, parent, false);
        binding.setHandler(handle);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ProductRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.binding.setProductRow(mValues.get(position));

        Glide.with(mContext)
                .load(holder.binding.getProductRow().getImg())
                .into(holder.binding.imgProduct);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final RowProductBinding binding;

        ViewHolder(final RowProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}

