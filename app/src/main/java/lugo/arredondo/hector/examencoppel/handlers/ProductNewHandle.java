package lugo.arredondo.hector.examencoppel.handlers;

/**
 * Created by Hector on 12/11/2017.
 */

public interface ProductNewHandle {
    void saveProduct();
    void showDialogPiker();
}
