package lugo.arredondo.hector.examencoppel.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static lugo.arredondo.hector.examencoppel.ProductNewActivity.REQUEST_TAKE_PHOTO;

/**
 * Created by Hector on 13/11/2017.
 */

public class FilesHelper {
    private final String TAG = FilesHelper.class.getSimpleName();

    private Context mContext;

    private File mFile = null;
    private String mCurrentPhotoPath;
    private int mWith = 1000;
    private int mHeight = 900;

    public FilesHelper(Context context) {
        this.mContext = context;
    }

    private FilesHelper() {
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File temp = File.createTempFile("temp", ".credi", mContext.getCacheDir());
        temp.mkdir();

        if (storageDir == null) {
            throw new IOException("External directori picture is null.");
        }

//        File storageDir = mContext.getCacheDir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    /**
     * From activity
     * Crea y lanza un intent para tomar fotografia
     */
    public void dispatchTakePictureIntent() throws NullPointerException, IOException {
        ((Activity) mContext).startActivityForResult(createIntentTakePictureIntent(), REQUEST_TAKE_PHOTO);
    }

    /**
     * From Fragment
     *
     * @return Intent, takePictureIntent
     */
    public Intent createIntentTakePictureIntent() throws NullPointerException, IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                mFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, "Error al crear photoFile", ex);
                throw ex;
            }
            // Continue only if the File was successfully created
            if (mFile != null) {
//                Uri photoURI = FileProvider.getUriForFile(mContext,
//                        "com.credilike.me.fileprovider",
//                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mFile));
                return takePictureIntent;
//                ((Activity) mContext).startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } else {
                Log.e(TAG, "File not created, intente is null");
                throw new NullPointerException("File no created");
            }
        }

        throw new NullPointerException("No camera activity to handle the intent");
    }

    public static String encodeTobase64(Bitmap image) {
        if (image != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();
            String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

            return imageEncoded;
        } else
            return null;
    }

    /**
     * Method used for encode the file to base64 binary format
     *
     * @param sFile
     * @return encoded file format
     */
    public static String encodeFileToBase64Binary(String sFile) {
        File file = new File(sFile);
        String encodedfile = null;

        if (!file.exists()) {
            Log.e("encodeFile", "File not found from: " + sFile);
            return null;
        }

        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int) file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = Base64.encodeToString(bytes, Base64.NO_WRAP);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return encodedfile;
    }

    public static Bitmap decodeBitmapFromPath(String path, int Width, int Height) {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
//        int scaleFactor = Math.min(photoW / Width, photoH / Height);
//        int scaleFactor = Math.round(photoW / Width);
        int scaleFactor = getScale(photoW, photoH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeFile(path, bmOptions);
    }

    public static String encodeImgBase46FromPath(String path) {
        Bitmap img = decodeBitmapFromPath(path, 0, 0);

        return encodeTobase64(img);
    }

    public Bitmap getPhoto() {
        // Get the dimensions of the View
        int targetW = mWith;
        int targetH = mHeight;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }

    public String getmCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public Uri getCurrentUri() {
        return Uri.fromFile(mFile);
    }

    private static int getScale(int inWidth, int inHeight) {
        final int REQUIRED_SIZE = 300;
        int width_tmp = inWidth, height_tmp = inHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

//        return (Math.min(width_tmp, height_tmp) / REQUIRED_SIZE);
        return scale;
    }

    public void resert() {
        this.mFile = null;
        this.mCurrentPhotoPath = "";
    }
}
