package lugo.arredondo.hector.examencoppel.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import lugo.arredondo.hector.examencoppel.models.Product;

/**
 * Created by Hector on 12/11/2017.
 */

@Dao
public interface ProductDAO {
    @Insert
    void saveAll(List<Product> products);
    @Insert
    void save(Product product);
    @Query("SELECT * from product")
    LiveData<List<Product>> findAll();
    @Query("SELECT * FROM product WHERE id = :id")
    LiveData<Product> find(int id);
    @Delete
    void delete(Product product);
    @Query("DELETE FROM product")
    void deleteAll();
}
