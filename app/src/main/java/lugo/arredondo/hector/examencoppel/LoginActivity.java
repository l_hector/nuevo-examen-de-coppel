package lugo.arredondo.hector.examencoppel;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import lugo.arredondo.hector.examencoppel.databinding.ActivityLoginBinding;
import lugo.arredondo.hector.examencoppel.handlers.LoginHandler;
import lugo.arredondo.hector.examencoppel.models.User;


public class LoginActivity extends AppCompatActivity implements LoginHandler {

    private final static String EXTRA_IMAGEN = "extraImagen";

    private User mUser;

    public static void NextActivity(Activity activity, ImageView imgLogo) {
        Intent i = new Intent(activity, LoginActivity.class);
        ActivityOptionsCompat option = ActivityOptionsCompat
                .makeSceneTransitionAnimation(activity, imgLogo,
                        EXTRA_IMAGEN);
        ActivityCompat.startActivity(activity, i, option.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        ViewCompat.setTransitionName(findViewById(R.id.img_logo), EXTRA_IMAGEN);

        mUser = new User("Hector", "123456");

        binding.setUser(mUser);
        binding.setHandler(this);

        binding.edtUser.setSelection(binding.edtUser.getText().toString().length());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void login(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
