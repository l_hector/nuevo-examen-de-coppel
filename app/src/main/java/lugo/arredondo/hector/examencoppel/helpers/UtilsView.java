package lugo.arredondo.hector.examencoppel.helpers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import lugo.arredondo.hector.examencoppel.R;

/**
 * Created by Hector on 12/11/2017.
 */

public class UtilsView {
    private final static String TAG = UtilsView.class.getSimpleName();

    private static final View.OnClickListener listenerDefault = view -> {
    };

    public static void handleProgressBar(@Nullable Resource resource, @NonNull ProgressBar progressBar) {
        if (resource == null) {
            progressBar.setVisibility(View.GONE);
            return;
        }

        progressBar.setVisibility(resource.status == Status.LOADING ? View.VISIBLE : View.GONE);
    }

    public static void showSnackBar(@NonNull Resource resource,
                                    @NonNull View view, View.OnClickListener listener) {
        switch (resource.status) {
            case Status.LOADING:
                //no hacer nada
                break;
            case Status.ERROR:
                if (listener == null) {
                    Log.e(TAG, "Listener retry no implemanteado");
                }

                showRetry(view, resource.message, listener);
                break;
            case Status.SUCCESS:
                showSnackBar(view, resource.message);
                break;

        }
    }

    public static void showRetry(@NonNull View view, @StringRes int title,
                                 @Nullable View.OnClickListener listener) {
        Snackbar snackbar = Snackbar.make(view, title, Snackbar.LENGTH_INDEFINITE);

        if (listener != null) {
            snackbar.setAction(R.string.retry, listener);
        }

        snackbar.show();
    }

    public static void showRetry(@NonNull View view, String title,
                                 @Nullable View.OnClickListener listener) {
        Snackbar snackbar = Snackbar.make(view, title, Snackbar.LENGTH_INDEFINITE);

        if (listener != null) {
            snackbar.setAction(R.string.retry, listener);
        }

        snackbar.show();
    }

    public static void showSnackBar (@NonNull View view, @Nullable Resource resource) {
        if (resource == null || resource.status == Status.LOADING) return;

        showSnackBar(view, resource.message);
    }

    public static void showSnackBar(@NonNull View view, String title) {
        Snackbar.make(view, title, Snackbar.LENGTH_LONG).show();
    }
}
