package lugo.arredondo.hector.examencoppel;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.IOException;

import lugo.arredondo.hector.examencoppel.databinding.ActivityProductNewBinding;
import lugo.arredondo.hector.examencoppel.handlers.ProductNewHandle;
import lugo.arredondo.hector.examencoppel.helpers.FilePath;
import lugo.arredondo.hector.examencoppel.helpers.FilesHelper;
import lugo.arredondo.hector.examencoppel.helpers.UtilsView;
import lugo.arredondo.hector.examencoppel.models.Product;
import lugo.arredondo.hector.examencoppel.models.views.ProductViewModel;

public class ProductNewActivity extends AppCompatActivity implements ProductNewHandle{
    private static final String TAG = ProductNewActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;
    public static final int REQUEST_TAKE_PHOTO = 102;
    public static final int REQUEST_CODE_GALERY_PICK = 103;

    private ActivityProductNewBinding binding;

    private Product mProduct;
    private ProductViewModel productViewModel;

    private FilesHelper filesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,
                R.layout.activity_product_new);
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setNavigationOnClickListener((View) -> onBackPressed());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mProduct = new Product();
        mProduct.setName("");
        mProduct.setDescripction("");
        binding.setProduct(mProduct);

        productViewModel = new ProductViewModel(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_new_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.acction_save) {
           saveProduct();
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void saveProduct() {
        if (TextUtils.isEmpty(binding.edtDescription.getText().toString())) {
            binding.edtDescription.setError(getString(R.string.required));
            binding.edtDescription.requestFocus();

            return;
        }

        if (TextUtils.isEmpty(binding.edtName.getText().toString())) {
            binding.edtName.setError(getString(R.string.required));
            binding.edtName.requestFocus();

            return;
        }

        productViewModel.saveProductLocal(mProduct).observe(this, resource -> {
            if (resource == null) return;

            UtilsView.handleProgressBar(resource, binding.progress);
            UtilsView.showSnackBar(resource, binding.root, view -> saveProduct());
        });
    }

    @Override
    public void showDialogPiker() {

    }

    public void showDialogPiker(View view) {
        Log.e(TAG, "onClickProduct photo");

        this.filesHelper = new FilesHelper(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.chose)
                .setItems(R.array.array_take_piker_tipies, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item

                        Intent intent = null;
                        int requestCode = 0;

                        switch (which) {
                            case 0: //Tomar foto
                                Intent intentTakePhoto = null;
                                try {
                                    //Intent intentTakePhoto =
                                    intentTakePhoto = filesHelper.createIntentTakePictureIntent();
                                    intentTakePhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (NullPointerException exn) {
                                    Log.e(TAG, "dispatchTakePictureIntent", exn);
                                }

                                startActivityForResult(intentTakePhoto, REQUEST_TAKE_PHOTO);
                                return;

//                                break;
                            case 1: //Seleccionar foto desde galeria
                                checkGalleryPermission();
                                break;
                        }
                    }
                });

        builder.create().show();
    }

    private void checkGalleryPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(ProductNewActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(ProductNewActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                    new AlertDialog.Builder(this)
                            .setTitle("Read external storage necesary")
                            .setMessage("Esta app necesita accesar a leer la memoria interna del dispositivo.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    ActivityCompat.requestPermissions(ProductNewActivity.this,
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                                }
                            }).create().show();

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(ProductNewActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                ActivityCompat.requestPermissions(ProductNewActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        } else {
            showIntentGalleryPhoto();
        }
    }

    private void showIntentGalleryPhoto() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        int requestCode = REQUEST_CODE_GALERY_PICK;

        //sets the select file to all types of files
        intent.setType("image/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Seleccione"), requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO:
//                    updateAdapterFileNomina(filesHelper.getmCurrentPhotoPath(), filesHelper.getCurrentUri(), true);
                    Uri uriTakePhoto = filesHelper.getCurrentUri();
                    Glide.with(this)
                            .load(uriTakePhoto)
                            .into(binding.imgPhoto);

                    break;
                case REQUEST_CODE_GALERY_PICK:
                    if (data == null) return;
                    Uri selectedFileUri = data.getData();
                    String selectedFilePath = FilePath.getPath(this, selectedFileUri);

                    if (TextUtils.isEmpty(selectedFilePath)) {
                        Toast.makeText(this, R.string.err_load_file, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Glide.with(this)
                                .load(selectedFileUri)
                                .into(binding.imgPhoto);
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    showIntentGalleryPhoto();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    checkGalleryPermission();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}