package lugo.arredondo.hector.examencoppel.services;

import lugo.arredondo.hector.examencoppel.models.User;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by Hector on 12/11/2017.
 */

public interface UserServices {
    @POST("user/login")
    Call<Boolean> login(User user);
}
