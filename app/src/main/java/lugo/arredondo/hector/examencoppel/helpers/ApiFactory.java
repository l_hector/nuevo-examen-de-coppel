package lugo.arredondo.hector.examencoppel.helpers;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hector on 12/11/2017.
 */

public class ApiFactory {
    private static final String urlBase = "https://dry-sands-38512.herokuapp.com";
    public static final String url = urlBase + "/api/";
    public static final String urlImg = urlBase + "";

    public static <T> T createInstance(Class<T> clazz) {
        return createInstance(clazz, url);
    }

    public static <T> T createInstance(Class<T> clazz, String url) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();

        return retrofit.create(clazz);
    }
}
