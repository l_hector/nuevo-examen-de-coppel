package lugo.arredondo.hector.examencoppel;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lugo.arredondo.hector.examencoppel.adapters.ProductRecyclerViewAdapter;
import lugo.arredondo.hector.examencoppel.databinding.ActivityMainBinding;
import lugo.arredondo.hector.examencoppel.handlers.ProductRowHandle;
import lugo.arredondo.hector.examencoppel.helpers.Status;
import lugo.arredondo.hector.examencoppel.helpers.Utils;
import lugo.arredondo.hector.examencoppel.helpers.UtilsView;
import lugo.arredondo.hector.examencoppel.models.Product;
import lugo.arredondo.hector.examencoppel.models.views.ProductViewModel;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

public class MainActivity extends AppCompatActivity implements ProductRowHandle {

    private ActivityMainBinding binding;
    private ProductViewModel productViewModel;
    private ProductRecyclerViewAdapter adapter;
    private List<Product> list;

    private boolean isUpdated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);

        list = new ArrayList<>();
        productViewModel = new ProductViewModel(this);
        adapter = new ProductRecyclerViewAdapter(this, list);
        productViewModel.init();
        binding.listProduct.setAdapter(adapter);
        binding.listProduct.setLayoutManager(new LinearLayoutManager(this, VERTICAL, false));

        binding.swipeRefresh.setOnRefreshListener(() -> {
            productViewModel.updateProduct().observe(this, resource -> {
                if (resource == null) return;

                binding.swipeRefresh.setRefreshing(resource.status == Status.LOADING);

                if (resource.status == Status.LOADING) {
                    list.clear();
                    adapter.notifyDataSetChanged();
                } else if (resource.status == Status.SUCCESS) {
                    isUpdated = true;
                } else if (resource.status == Status.ERROR) {
                    Toast.makeText(this, resource.message, Toast.LENGTH_SHORT).show();
                }
            });
        });

        updateUI();
    }

    public void updateUI() {
        productViewModel.getProducts().observe(this, products -> {
            binding.swipeRefresh.setRefreshing(false);
            updateUI(products);
        });
    }

    private void updateUI(List<Product> products) {
        products = products != null ? products : new ArrayList<>();
        list.clear();

        if (!isUpdated) {
            if (products.size() > 0) {
                for (int i = 0; i < products.size(); i++) {
                    products.get(i).setLocal(true);
                }
            }
        }

        list.addAll(products);

        binding.notProduct.setVisibility(products.size() > 0 ? View.GONE : View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.acction_new) {
            startActivity(new Intent(this, ProductNewActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void deleteProduct(Product item) {
        new AlertDialog.Builder(this)
                .setTitle("Borrar producto")
                .setMessage("¿Estas seguro que queires borrar este producto?")
                .setPositiveButton("Borrar", (v, i) -> productViewModel.deleteProduct(item).observe(this, resource -> {
                            UtilsView.handleProgressBar(resource, binding.progress);
                            UtilsView.showSnackBar(binding.root, resource);
                        }
                )).show();
    }

    @Override
    public void onClickProduct(Product item) {
        Toast.makeText(this, "click: " + item.getName(), Toast.LENGTH_SHORT).show();
    }
}
