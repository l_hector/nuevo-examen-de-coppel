package lugo.arredondo.hector.examencoppel.helpers;

/**
 * Created by Hector on 12/11/2017.
 */

public interface Status {
    int LOADING = -1;
    int SUCCESS = 0;
    int ERROR = 1;
}
