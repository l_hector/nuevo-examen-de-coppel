# README #

Realizar un servicio REST en el lenguaje que desee el programador, publicandolo para poder funcionar la App correctamente.
Este servicio REST debera contener los siguientes endpoints (todos los endpoint a excepcion del login recibiran en el header el jwt para validar que el usuario se haya logueado):

## 1.- Login.- ##
	Aqui el usuario se logueara ya sea conectandose a una BD o directamente con un usuario y contraseña definidos, si son correctos retornara un json con JWT y el nombre del usuario

## 2.- Listar productos.- ##
	Se retornara un listado de productos los cuales tendran la siguiente definicion:
		> Id
		> Nombre
		> Imagen (URL)
		> Descripcion

## 3.- Agregar producto.- ##
	Se agregara el producto al listado.

## 4.- Eliminar producto.- ##
	Se eliminara un producto del listado.


# Realizar una aplicacion movil (Android), conteniento las siguientes definiciones: #

## 1.- Login, ##
	se comunicara con el servicio rest y si todo sale bien continuara con la pantalla de listado de productos, en caso contrario se mostrara un error.

En la siguiente actividad se manejara un navegador donde se mostrara en su menu las siguientes opciones:

## 1.- Listado de productos.- ##
	En esa vista se mostraran los productos obtenidos del servicio REST

## 2.- Agregar producto.- ##
 Sera un formulario el cual se llenara con los datos del producto, asi como tomar la foto al producto y pasarlo al servicio REST

## 3.- Eliminar producto.- ## 
	Sera un formulario donde se seleccionara un producto y se eliminara en el servicio REST

La aplicacion movil deberá llevar las siguientes características:

	- Hacer uso de LiveData, ViewModel y Room para el manejo de la información.

	- DataBinding.

	- MVP

	- Dagger 2

	- Retrofit

	- Funciones Lambda

Si el servicio REST es en Node debera llevar las siguientes caracteristicas:

- ES2016

- Arrow functions